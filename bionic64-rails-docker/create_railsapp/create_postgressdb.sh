#!/usr/bin/env bash

sudo -i -u postgres psql
create role hakase_dev with createdb login password 'hakasepass';
\du
\q