#!/bin/bash

docker-compose run --no-deps web rails new . --force --database=postgresql
sudo chown -R $USER:$USER .
cat ./database.yml > ./config/database.yml
docker-compose up -d 
docker-compose run web rake db:create

rm database.yml 